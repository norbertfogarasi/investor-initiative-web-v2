export function isSameObject(obj1, obj2) {
    return JSON.stringify(obj1) == JSON.stringify(obj2);
}

export function containsObject (arr, obj) {
    let contains = false;
    arr.forEach(item => {
        if (JSON.stringify(item) == JSON.stringify(obj)) {
            contains = true;
        }
    });
    return contains;
}
