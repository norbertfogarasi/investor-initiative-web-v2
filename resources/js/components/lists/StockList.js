import React, {Component} from 'react'
import StockListItem from '../items/StockListItem';

export default class StockList extends Component {
    render() {
        const stocks = this.props.stocks.map(stock => {
            return <StockListItem key={stock.symbol} stock={stock} onSelectStock={this.props.onSelectStock} onRemoveStock={this.props.onRemoveStock} selectedStocks={this.props.selectedStocks}/>
        });

        return (
            <div className="card card-sec m-b-30 h-100 mx-auto">
                <div className="card-body">
                    <h4 className="mt-0 header-title">List of Stocks</h4>
                    <ul className="list-inline widget-chart m-t-20 text-center">
                        <li>
                            <h4 className=""><b>500</b></h4>
                            <p className="text-muted m-b-0">Company</p>
                        </li>
                        <li>
                            <h4 className=""><b>505</b></h4>
                            <p className="text-muted m-b-0">Stock</p>
                        </li>
                        <li>
                            <h4 className=""><b>1</b></h4>
                            <p className="text-muted m-b-0">Portfolio</p>
                        </li>
                    </ul>
                    <div className="d-flex align-content-center flex-wrap justify-content-center">
                        {stocks}
                    </div>
                </div>
            </div>
        );
    }
}

