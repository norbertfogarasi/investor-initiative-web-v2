import React, { Component } from 'react'
import SelectedStockListItem from '../items/SelectedStockListItem';
import Modal from '../elements/Modal';
import axios from 'axios';

export default class SelectedStockList extends Component {
    state = {
        portfolio: 'Loading'
    }

    renderOptimizeButton = () => {
        if(this.props.stocks !== undefined && this.props.stocks.length > 0) {
            return <button className="btn btn-primary btn-lg btn-block " onClick={() => {this.optimizePortfolio()}} data-toggle="modal" data-target="#exampleModal">Optimize</button>
        } else {
            return null;
        }
    }

    renderModalContent = () => {
        console.log('type', typeof this.state.portfolio)
        if(typeof this.state.portfolio === 'object') {
            return Object.keys(this.state.portfolio)
                .map(key => { 
                    return <p>{key + ' : ' + this.state.portfolio[key] + '%'}</p>
                })
        } else {
            return <p>{this.state.portfolio}</p>
        }
    }

    optimizePortfolio = () => {
        console.log('Sending POST request');
        axios.post('http://127.0.0.1:5000/api/v1/optimize/', this.props.stocks)
            .then(response => {                
                this.setState({portfolio: response.data})
            });
    }

    render() {
        const selectedStocks = this.props.stocks.map(stock => {
            return <SelectedStockListItem key={stock.symbol} stock={stock} onRemoveStock={this.props.onRemoveStock}/>
        });

        const actions = (
            <React.Fragment>
                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </React.Fragment>
        );

        return (
            <React.Fragment>
                <div className="card card-sec m-b-30 h-100">
                    <div className="card-body">
                        <h4 className="mt-0 m-b-15 header-title">Selected Stocks</h4>
                        <div style={{'overflow': 'auto', 'height': '800px'}}>
                            <div className="table-responsive">
                                <table className="table table-hover mb-0">
                                    <thead>
                                        <tr className="titles">
                                            <th>Symbol</th>
                                            <th>Sector</th>
                                            <th>Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {selectedStocks}
                                    </tbody>
                                </table>
                                {this.renderOptimizeButton()}
                            </div>
                        </div>
                    </div>
                </div>
                <Modal 
                    title="Result"
                    body={this.renderModalContent()}
                    actions={actions}/>
            </React.Fragment>
        );
    }
}

