import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import StockList from './lists/StockList';
import SelectedStockList from './lists/SelectedStockList';
import Pagination from './elements/Pagination';
import axios from 'axios';
import {containsObject, isSameObject} from '../utils/arrayUtils';

export default class PortfolioOptimization extends Component {
    state = { allStocks: [], currentStocks: [], currentPage: null, totalPages: null, selectedStocks: [] }

    componentDidMount() {
        axios.get('http://127.0.0.1:5000/api/v1/resources/stocks')
            .then(res => this.setState({ allStocks: res.data }))
    }

    onPageChanged = data => {
        const { currentPage, totalPages, pageLimit } = data;
        const offset = (currentPage - 1) * pageLimit;

        axios.get(`http://127.0.0.1:5000/api/v1/resources/stocks?limit=${pageLimit}&offset=${offset}`)
            .then(response => {
                const currentStocks = response.data;
                this.setState({ currentPage, currentStocks, totalPages });
            });
    }

    onSelectStock = (e, stock) => {
        e.preventDefault();
        if(!containsObject(this.state.selectedStocks, stock)) {
            this.setState({ selectedStocks: [...this.state.selectedStocks, stock] });
        }
    }

    onRemoveStock = (e, stock) => {
        e.preventDefault();
        this.setState({
            selectedStocks: this.state.selectedStocks.filter(item => !isSameObject(item, stock))
        })
    }

    render() {
        const { allStocks, currentStocks, currentPage, totalPages } = this.state;
        const totalStocks = allStocks.length;

        if (totalStocks === 0) return null;

        return (
            <div className="wrapper">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="page-title-box">
                                <h4 className="page-title">Portfolio Optimization</h4>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-xl-8 col-md-8 col-sm-8">
                            <StockList 
                                stocks={this.state.currentStocks}
                                selectedStocks={this.state.selectedStocks} 
                                onSelectStock={this.onSelectStock} 
                                onRemoveStock={this.onRemoveStock}/>
                        </div>
                        <div className="col-xl-4 col-md-4">
                            <SelectedStockList 
                                stocks={this.state.selectedStocks}
                                onRemoveStock={this.onRemoveStock}/>
                        </div>
                    </div>
                    <div className="d-flex flex-row py-4 align-items-center">
                        <Pagination 
                            totalRecords={totalStocks} 
                            pageLimit={12} 
                            pageNeighbours={2}
                            onPageChanged={this.onPageChanged}/>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById('portfolioRoot')) {
    ReactDOM.render(<PortfolioOptimization />, document.getElementById('portfolioRoot'));
}
