import React, {Component} from 'react';
import ReactDOM from 'react-dom'

export default class Modal extends Component {

    render() {
        return ReactDOM.createPortal(
            <div>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{this.props.title}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {this.props.body}
                        </div>
                        <div class="modal-footer">
                            {this.props.actions}
                        </div>
                        </div>
                    </div>
                </div>
            </div>,
            document.querySelector('#modal')
        );
    }
}


 