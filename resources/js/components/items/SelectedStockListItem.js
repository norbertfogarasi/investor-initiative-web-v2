import React, { Component } from 'react';

export default class SelectedStockListItem extends Component {
    render() {
        return (
            <tr>
                <td className="c-table__cell">
                    <div className="user-wrapper">
                        <div className="text-user">
                            <h6>{this.props.stock.symbol}</h6>
                            <p>{this.props.stock.name}</p>
                        </div>
                    </div>
                </td>
                <td className="c-table__cell">{this.props.stock.sector}</td>
                <td className="c-table__cell"><button className="btn btn-danger" onClick={(e) => {this.props.onRemoveStock(e, this.props.stock)}}>Remove</button></td>
            </tr>
        );
    }
}
