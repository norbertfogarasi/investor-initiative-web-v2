import React, {Component} from 'react';
import {containsObject} from '../../utils/arrayUtils';

export default class StockListItem extends Component {
    isSelected = () => {
        return containsObject(this.props.selectedStocks, this.props.stock);
    }

    renderButton = () => {
        if (this.isSelected()) {
            return <a href="#" className="btn btn-danger" onClick={(e) => {this.props.onRemoveStock(e, this.props.stock);}}>Remove</a>
        } else {
            return <a href="#" className="btn btn-primary" onClick={(e) => {this.props.onSelectStock(e, this.props.stock);}}>Add</a>
        }
    }

    render() {
        return (
            <div className={"card border text-center mx-auto mb-3 " + (this.isSelected() ? 'bg-success border-danger' : 'border-primary')} style={{'width': '15rem', 'height': '10rem', 'textOverflow': 'ellipsis', 'whiteSpace': 'nowrap', 'overflow': 'hidden'}}>
                <div className="card-body">
                    <h5 className="card-title">{this.props.stock.symbol}</h5>
                    <p className="card-text">{this.props.stock.name}</p>
                    {this.renderButton()}
                </div>
            </div>
        );
    }
}

