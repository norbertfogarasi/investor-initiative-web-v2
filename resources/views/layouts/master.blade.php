<!DOCTYPE html>
<html>
    @include('partials.head')

    <body>
        @include('elements.loader')
        @include('partials.navbar')
        @yield('content')
        @include('partials.footer')
    </body>

</html>
