@extends('layouts.master')

@section('content')
<div class="wrapper">
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title">Home</h4>
                </div>
            </div>
        </div>
        <!-- end page title end breadcrumb -->

        <div class="row">
            <div class="col-md-6 col-xl-3">
                <div class="mini-stat clearfix bg-primary">
                    <span class="mini-stat-icon"><i class="chess icon"></i></i></span>
                    <div class="mini-stat-info text-right text-light">
                        <span class="counter text-white">500</span> Companies
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-3">
                <div class="mini-stat clearfix bg-success">
                    <span class="mini-stat-icon"><i class="ti-user"></i></span>
                    <div class="mini-stat-info text-right text-light">
                        <span class="counter text-white">505</span> Stocks
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-3">
                <div class="mini-stat clearfix bg-danger">
                    <span class="mini-stat-icon"><i class="ti-shopping-cart-full"></i></span>
                    <div class="mini-stat-info text-right text-light">
                        <span class="counter text-white">1</span> Portfolio
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-3">
                <div class="mini-stat clearfix bg-info">
                    <span class="mini-stat-icon"><i class="ti-stats-up"></i></span>
                    <div class="mini-stat-info text-right text-light">
                        <span class="counter text-white">20544</span> New Projects
                    </div>
                </div>
            </div>
        </div>
        <!-- end of row -->

        <div class="row">
            <div class="col-xl-8">
                <div class="card card-sec m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 m-b-15 header-title">Selected Stocks</h4>

                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead>
                                    <tr class="titles">
                                        <th>Symbol</th>
                                        <th>Company's name</th>
                                        <th>Remove</th>
                                    </tr>

                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="c-table__cell">
                                            <div class="user-wrapper">
                                                <div class="img-user">
                                                    <img src="{{asset('images/users/user-1.jpg')}}" alt="user"
                                                        class="rounded-circle">
                                                </div>
                                                <div class="text-user">
                                                    <h6>AAPL</h6>
                                                    <p>Apple Inc.</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="c-table__cell"><span class="badge badge-warning">Remove</span></td>
                                    </tr>
                                    <tr>
                                        <td class="c-table__cell">
                                            <div class="user-wrapper">
                                                <div class="img-user">
                                                    <img src="{{asset('images/users/user-2.jpg')}}" alt="user"
                                                        class="rounded-circle">
                                                </div>
                                                <div class="text-user">
                                                    <h6>F</h6>
                                                    <p>Ford</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td><span class="badge badge-info">Remove</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4">
                <div class="card card-sec m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Monthly Earnings</h4>

                        <ul class="list-inline widget-chart m-t-20 text-center">
                            <li>
                                <h4 class=""><b>3654</b></h4>
                                <p class="text-muted m-b-0">Marketplace</p>
                            </li>
                            <li>
                                <h4 class=""><b>954</b></h4>
                                <p class="text-muted m-b-0">Last week</p>
                            </li>
                            <li>
                                <h4 class=""><b>8462</b></h4>
                                <p class="text-muted m-b-0">Last Month</p>
                            </li>
                        </ul>

                        <div id="morris-donut-example" style="height: 265px"></div>
                    </div>
                </div>
            </div>

        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->
@endsection
