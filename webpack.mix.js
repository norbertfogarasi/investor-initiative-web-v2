const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/app.scss', 'public/css')
    .styles([
        'resources/css/icons.css',
        'resources/css/style.css',
        ],
    'public/css/app.css')
    .js([
        'resources/js/popper.min.js',
        'resources/js/waves.js',
        'resources/js/main.js',
        ],
    'public/js/app.js')
    .react('resources/js/app.js', 'public/js/app.js');
